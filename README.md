# Description

Test results using command php vendor/bin/codecept run --steps : <br>

Acceptance Tests (5) ----------------------------------------------------------- <br>
DirLoginCest: Successfull login <br>
Signature: DirLoginCest:successfullLogin <br>
Test: tests/acceptance/DirLoginCest.php:successfullLogin <br>
*Scenario* -- <br> 
 I am on page "/login" <br>
 I fill field "#login-email","alexandurpetrov94@abv.bg" <br>
 I fill field "#password","nXjjpmACv8" <br>
 I click "//input[@value='Влез']" <br>
 I see in current url "/profile" <br>
 I see "Профил" <br>
 **PASSED**  <br>

DirLoginCest: Login with wrong email <br>
Signature: DirLoginCest:loginWithWrongEmail <br>
Test: tests/acceptance/DirLoginCest.php:loginWithWrongEmail <br>
*Scenario* -- <br>
 I am on page "/login" <br>
 I fill field "#login-email","alexandurpetrov94abv.bg" <br>
 I fill field "#password","nXXXXXjjpmACv8" <br>
 I click "//input[@value='Влез']" <br>
 I see "Невалидни данни за достъп. Грешен E-mail или парола." <br>
 I don't see "zxander" <br>
**PASSED**  <br>

DirLoginCest: Login with wrong password <br>
Signature: DirLoginCest:loginWithWrongPassword <br>
Test: tests/acceptance/DirLoginCest.php:loginWithWrongPassword <br>
*Scenario* -- <br>
 I am on page "/login" <br>
 I fill field "#login-email","alexandurpetrov94@abv.bg" <br>
 I fill field "#password","nXXXXXjjpmACv8" <br>
 I click "//input[@value='Влез']" <br>
 I see "Невалидни данни за достъп. Грешен E-mail или парола." <br>
 I don't see "zxander" <br>
**PASSED**  <br>

DirLoginCest: Login without email <br>
Signature: DirLoginCest:loginWithoutEmail <br>
Test: tests/acceptance/DirLoginCest.php:loginWithoutEmail <br>
*Scenario* -- <br>
 I am on page "/login" <br>
 I fill field "#login-email","" <br>
 I fill field "#password","nXXXXXjjpmACv8" <br>
 I click "//input[@value='Влез']" <br>
 I see "Полето е задължително." <br>
 I don't see "zxander" <br>
**PASSED** <br>

DirLoginCest: Login without email and password <br>
Signature: DirLoginCest:loginWithoutEmailAndPassword <br>
Test: tests/acceptance/DirLoginCest.php:loginWithoutEmailAndPassword <br>
*Scenario* -- <br>
 I am on page "/login" <br>
 I fill field "#login-email","" <br>
 I fill field "#password","" <br>
 I click "//input[@value='Влез']" <br>
 I see "Полето е задължително." <br>
 I don't see "zxander" <br>
**PASSED**  <br>


Time: 00:04.392, Memory: 10.00 MB

OK (5 tests, 10 assertions)
