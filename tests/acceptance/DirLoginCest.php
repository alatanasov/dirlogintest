<?php 

class DirLoginCest
{
    public function successfullLogin(AcceptanceTester $I)
    {
    	$I->amOnPage('/login');
		$I->fillField('#login-email', 'alexandurpetrov94@abv.bg');
		$I->fillField('#password', 'nXjjpmACv8');
		$I->click("//input[@value='Влез']");
		$I->seeInCurrentUrl('/profile');
		$I->see('Профил');
    }

    public function loginWithWrongEmail(AcceptanceTester $I)
    {
    	$I->amOnPage('/login');
		$I->fillField('#login-email', 'alexandurpetrov94abv.bg');
		$I->fillField('#password', 'nXXXXXjjpmACv8');
		$I->click("//input[@value='Влез']");
		$I->see('Невалидни данни за достъп. Грешен E-mail или парола.');
		$I->dontSee('zxander');
    }


    public function loginWithWrongPassword(AcceptanceTester $I)
    {
    	$I->amOnPage('/login');
		$I->fillField('#login-email', 'alexandurpetrov94@abv.bg');
		$I->fillField('#password', 'nXXXXXjjpmACv8');
		$I->click("//input[@value='Влез']");
		$I->see('Невалидни данни за достъп. Грешен E-mail или парола.');
		$I->dontSee('zxander');
    }

    public function loginWithoutEmail(AcceptanceTester $I)
    {
    	$I->amOnPage('/login');
		$I->fillField('#login-email', '');
		$I->fillField('#password', 'nXXXXXjjpmACv8');
		$I->click("//input[@value='Влез']");
		$I->see('Полето е задължително.');
		$I->dontSee('zxander');
    }

    public function loginWithoutEmailAndPassword(AcceptanceTester $I)
    {
    	$I->amOnPage('/login');
		$I->fillField('#login-email', '');
		$I->fillField('#password', '');
		$I->click("//input[@value='Влез']");
		$I->see('Полето е задължително.');
		$I->dontSee('zxander');
    }
}


